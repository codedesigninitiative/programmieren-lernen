// start: helper for getting user input
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})
// stop: helper for getting user input

var goal_number, inp;
goal_number = 100;

function check_number(x) {
    if ((x === goal_number)) {
        console.log("congratulations. You won, Your number is right");
    } else {
        if ((goal_number < x)) {
            console.log("the number is lower");
        } else {
            console.log("the number is higher");
        }
    }
}

readline.question(`guess the number: `, (inp) => {
  check_number(Number.parseInt(inp));
  readline.close()
})

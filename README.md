# Learn how to code

This is your first game, written in the Python programming language (in brackets you find the same in the Ruby programming language)

- Open [this link](https://repl.it/repls/WarmheartedUncommonApplicationstack) to open the game ([Ruby version](https://repl.it/@MartinBetz/SerpentineFlusteredNaturallanguage))
- The game is already complete and works
- Click the green run button to test the game
- In line 1, change the number `100` to one that you like
- Hide the game's code ([see here](https://s.put.re/HHpRCc6N.gif) how to do it)
- Hand your computer to your right neighbour
- Run the game again with the run button
- Let the neighbor guess the number that you like
 
# Ideas for changing the game
- When user is right game print message with variable: "You won. NUMBER is right"
  - Learn from [this example](https://cito.github.io/blog/f-strings/) (_Ruby:_ Follow [this tutorial](http://ruby-for-beginners.rubymonstas.org/bonus/string_interpolation.html))
- User has multiple guesses
  - Google: `for loop "python 3"` (_Ruby:_ Google `for loop ruby`)
- Let Python (Ruby) choose a random number between 1 and 100
  - Learn from [this example](https://pythonprogramminglanguage.com/randon-numbers/) (_Ruby:_ just use `rand 200` to get random number from 0 to 200)
- Print error message when user enters word instead of number
  - Google for `check if number "python 3"` (_Ruby:_ Use [this answer](https://stackoverflow.com/a/4589985) for Ruby)
